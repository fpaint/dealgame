<?php

$request = isset($_GET['request']) ? $_GET['request'] : 'home';
show_page($request);

//----------- Наношаблонизатор ---------------------------
function fetch_template($name, $data = array()) {
  extract($data);
  ob_start();  
  include('templates/'.$name);
  $text = ob_get_contents();  
  ob_end_clean();
  return $text;
}

function show_page($request) {
  $content = fetch_template($request.'.php');
  print fetch_template('main_layout.php', array('content'=>$content));
}

?>