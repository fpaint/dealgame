<?= fetch_template('partials/orange_box.php', array('title'=>'Контакты', 'class'=>'container')) ?>

<div class='contacts container'>
  <div class='alert'>По любым вопросам, связанным с перечислением денежных средств, пополнением игрового счета, работой сайта, Вы можете
связаться со Злобным Гарри.</div>
  <div class='row bottom_gap'>
    <div class='col-md-3'>
      <div class='icon icon-icq'>ICQ: 446-168-131</div>
      <div class='icon icon-skype'>Skype: ZlobnyGArry</div>        
    </div>
    <div class='col-md-9'>
      e-mail: bank(at)dealgame.ru<br/>
      e-mail: support(at)dealgame.ru
    </div>
  </div>
  <div class='row'>
    <div class='col-md-6'>
      <p>Форма обратной связи:</p>
      <form>
        <div class='form-group'>
          <label for='email'>Ваш e-mail:</label>
          <input type='text' name='email' class='form-control'>
        </div>
        <div class='form-group'>
          <label for='text'>Текст сообщения:</label>
          <textarea name='text' class='form-control'></textarea>
        </div>
        <a href='' class='green'>Отправить сообщение</a>
      </form>
    </div>  
  </div>  
</div>