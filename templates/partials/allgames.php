<?php
  function print_game() { ?>
    <div class='game_item'>
      <a href='/pay'>
        <img src='/assets/img/games/rage.jpg' />
        <div>
          www.game.ru
        </div>  
      </a>
    </div>
  <?php
  }
?>

<?= fetch_template('partials/orange_box.php', array('title'=>'Все игры', 'link'=>'Популярные игры', 'link_href'=>'/')) ?>

<div class='games allgames'>
  <form>
     <div class="form-group">
      <input type='text' class="form-control" name='search' placeholder='Для того, что бы найти игру, введите ее название в это поле' />
     </div> 
  </form>
  <div class='row'>
  <?php for($j=0;$j<20;$j++) { ?>
    <div class='game col-lg-2 col-md-3 col-sm-4 col-xs-4'>
      <? print_game(); ?>
    </div>  
  <?php } ?>
  </div>
</div>