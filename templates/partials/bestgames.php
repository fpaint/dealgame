<?= fetch_template('partials/orange_box.php', array('title'=>'Популярные игры', 'link'=>'Все игры', 'link_href'=>'/allgames')) ?>

<div class='games bestgames'>
  <div class='gamerow row'>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>    
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>    
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>
    <div class='game col-md-4 col-xs-6'><img src='/assets/img/games/dragon.jpg'></div>    
  </div>
</div>

<script>

$(function() {
  $('.bestgames').on('mouseenter', '.game', function() {
    $(this).append("<div class='shade'><a href='/pay'>Внести платёж в игру</a></div>");
  })
  .on('mouseleave', '.game', function() {
    $('.shade', this).remove();
  });
});

</script>