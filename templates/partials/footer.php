<div class='footer'>
  <div class='container'>
    <div class='row'>
      <div class='copyright col-md-3 col-sm-6'>
        <div class='item'>© 2008–2014 DealGame</div>
        <div class='item'><a href=''>Уведомление о рисках (WebMoney)</a></div>
        <div class='item'>При возникновении вопросов, связанных с переводом денежных средств, или работой сайта, просьба обращаться к <a href=''>Злобному Гарри.</a></div>
      </div>
      <div class='games-list col-md-3 col-sm-6'>
        У нас Вы можете оплатить онлайн игры или получить дополнительные возможности в играх, посредством смс-подписок, произвести активацию через смс, осуществить онлайн оплату игр: Легенда: Наследие драконов (фэо-прайм), Легенда: Наследие драконов (фэо-минор), Легенда: Наследие драконов (фэо-медиант), Легенда: Наследие драконов (фэо-квад), Джаггернаут, Троецарствие, Perfect World, Территория, Герои: Возрождение, Бойцовский клуб (Enchanter), Территория 2, Flyff, Жуки@Mail.Ru, Berserk-Online, Властелин Колец Онлайн, ARENA Online: Dragon Age - Нор Лаед, ARENA Online: Dragon Age - Нулу Кадар, Техномагия, Драйв@Mail.ru, Carnage, World of Warcraft, COSMICS: Галактические войны, Десант: Рейдеры Мериона, Пара Па, RF Online, R2 Online, Lineage 2, Ace Online, Fortress 2, 11x11 - Футбольный менеджер, Гладиаторы, Ботва Онлайн 1, Онлайн бильярд, NeverLands, LoveCity, Ботва Онлайн 2, U.N.I.T., Motorwars, Черновик, Sky2Fly, Танки Онлайн, Klanz, Last Combat: Последняя битва, Дом 3, Webracing, Сфера: Перерождение,
      </div>
      <div class='games-list col-md-3 col-sm-6'>
        Сфера 2 Арена, Rappelz, Фаор, Last Chaos, Infinuum, Территория футбола, Аллоды Онлайн, Понаехали тут, Silk Road, Jade Dynasty, Пиратия, TimeZero, Granado Espada: Вызов Судьбы, Destiny, Bloody world, Хранители силы, Северный клинок, DealGame, Королевство 1, Ганжубасовые войны, Lava Online, Годвилль, Фрагория, ARENA Ultra-Воронграй, BananaWars, Magic, Сфера, Сфера Судьбы, Virtown, Mega Racers, PIG.RU, D.O.T., Ботва Онлайн 3, Битва Героев [bitva-geroev.ru] Snail Game, Видеочат viChatter, World of Tanks, Дорога через Хаос - Творцы миров, FreeStyle, PointBlank, Aion, BNB, Bomjionline, Предел Онлайн - битва Ярости и Удачи, Короли льда, Веселый цирк Алле Оп, Повелители драконов, GAMEXP, Фантазиум, Lost Magic, Hellgard, Cross Fire, Battle of the Immortals, Business Tycoon Online, Полный Пи, RIOT, WarFace, БУМЗ, Небеса, Karos: Начало, Dragon Nest, Drako, Осада Онлайн, Герои войны и денег, Магические Земли, Вконтакте, Ботва Турбо, 8-day, Вконтакте, Поднебесье, Bed Pets, Prime World, InetBall.ru, и другие.
      </div>
      <div class='games-list col-md-3 col-sm-6'>
        Для Вашего удобства прием платежей осуществляется с помощью WebMoney WMZ, WebMoney WMR, WebMoney WME, WebMoney WMB, WebMoney WMU, WebMoney WMY, Яндекс.Деньги, Банковские карты, RBK Money, Карты Webmoney, SMS, Карты ДеньгиOnline, MoneyMail, QIWI Кошелек, WebCreds, EasyPay, Платежи пользователей, ОСМП, W1, Platezh.Ru, Олди-Т, Банковские карты (EUR), Банковские карты (USD), SMS Plastic Media, SMS Movable, ExpressGold, Platezh.Ru - карты, IntellectMoney, Z-Payment, Деньги@Mail.Ru, iFree Подписки, PayPal, EasySoft, Money-Money, Свободная касса, SMS INFON, LiqPay, Элекснет, Интеркасса, W1, НСМЭП UAH, MoneyMail, Уникарта, Liberty Reserve, PerfectMoney, WebCreds, Telemoney, Z-Payment, MoneyXY, Бонус, Мобильный Платеж (МТС Beeline Мегафон), PayPal, MoneyBookers Skrill, Альфа-Банк, ВТБ 24, Сбербанк, Банк Авангард, Приват-Банк Украина, Чеки Ukash (Европа), TauLink (Казахстан), Терминалы I-Box (Украина), Переводы CONTACT, Переводы Migom, Переводы MoneyGram, Переводы WesternUnion, Переводы Юнистрим, Терминалы Уникасса, Переводы Почты России, QIWI Кошелек, Мобильный кошелек QIWI, и другие.
      </div>

    </div>
  </div>
</div>
