<div class='container'>
  <div class='top_header'>
    <div class='row'>
      <div class='col-md-3'>
        <a href='/'><img src='/assets/img/logo.png' /></a>
      </div>
      <div class='col-md-5'>
        Онлайн-консультант:<br />
        <span class='icon icon-icq right-gap'>ICQ: 446-168-131</span>
        <span class='icon icon-skype'>Skype: ZlobnyGArry</span>        
      </div>      

    </div>
  </div>
</div>
<div class='main_menu'>
  <div class='container'>
    <div class='row'>
      <div class='col-md-10 col-xs-9'>
        <ul>
          <li class='active'><a href='/'>Главная</a></li>
          <li><a href='/catalog'>Каталог игр</a></li>
          <li><a href='/sms'>SMS-сервис</a></li>
          <li><a href='/news'>Новости</a></li>
          <li><a href='/contacts'>Контакты</a></li>
          <li><a href='/dealers'>Представители</a></li>
        </ul>
      </div>
      <div class='col-md-2 col-xs-3'>
        <a href='/'>Вход для клиентов</a>
      </div>  
    </div>      
  </div>  
</div>
<div class='clearfix'></div>