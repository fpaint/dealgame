<div class='orange_box <?= isset($class) ? $class : '' ?>'>
  <div class='orange_title'>
    <div class='left_tail'></div>
    <div class='center_block'>
      <div class='active'><?= $title ?></div><? if(isset($link)) { ?><div class='link'><a href='<?= $link_href ?>'><?= $link ?></a></div><? } ?>
    </div>
    <div class='right_tail'></div>
  </div>
</div>  
