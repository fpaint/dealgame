<div class='payment container'>  

  <? include('templates/partials/stages.php') ?>  

  <?= fetch_template('partials/orange_box.php', array('title'=>'Выберите наиболее удобный для вас способ оплаты', 'link'=>'Предыдущий шаг', 'link_href'=>'/allgames')) ?>  

  <div class='pay_info row'>
    <div class='col-md-3'>      
      <img src='/assets/img/games/dragon.jpg' class='full_width'>
    </div>

    <div class='col-md-6 p_gaps'>
      <p>Вы хотите приобрести игровую валюту в игре:<br />
         <strong>Легенда: Наследие драконов (фэо-прайм)</strong>
      </p>   

      <p>Выбраный способ платежа:<br />
      <strong class='green'>Мобильный Платеж (МТС Beeline Мегафон)</strong>
      </p>

      <p>
        <img src='/assets/img/wmz.png' class='right-gap'>
        <a href='/pay2' class='green'>Подтвердить платёж</a>
      </p>

      <p class='gray'>
        Cтоимость 1-ой игровой единицы в проекте Легенда: Наследие драконов (фэо-прайм) составляет 20 RUR
      </p>

    </div>

    <div class='col-md-3 gray'>
      Mир Фэо. Красивый и загадочный. Мир, в котором никогда не прекращается война. Две расы ведут ожесточенную битву за право жить в этом мире. Принимайте сторону одной из них и вас ждут красивейшие бои с врагами, увлекательные походы на монстров, захватывающие квесты. В игре "Легенда: Наследие Драконов" Вы найдете все, что так любите в онлайн играх.
    </div>

  </div>
  
  <div class='pay_methods'>
    <a href='' class='gray'>Электронные платежи</a>
    <a href='' class='gray'>Банковские переводы</a>
    <a href='' class='orange'>Другие переводы</a>
  </div>

  <div class='pay_services'>
    <div class='item discount'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item discount'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item discount'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item discount'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>  
    <div class='item'>
      <img src='/assets/img/contact_pay.png'>
    </div>    
  </div>
  <div class='clearfix'></div>

</div>  