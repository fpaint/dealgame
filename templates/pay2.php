<div class='payment container'>  
  <? include('templates/partials/stages.php') ?>  

  <div class='orange_box'>
    <div class='orange_title'>
      <div class='left_tail'></div>
      <div class='active'></div>
      <div class='link'><a href='/pay'>Предыдущий шаг</a></div>
      <div class='right_tail'></div>
    </div>
  </div>  

  <?= fetch_template('partials/orange_box.php', array('title'=>'Укажите имя персонажа и сумму покупки', 'link'=>'Предыдущий шаг', 'link_href'=>'/pay')) ?>  

  <div class='pay_info row'>
    <div class='col-md-3'>      
      <img src='/assets/img/games/dragon.jpg' class='full_width'>
    </div>

    <div class='col-md-6 p_gaps'>
      <p>Вы хотите приобрести игровую валюту в игре:<br />
         <strong>Легенда: Наследие драконов (фэо-прайм)</strong>
      </p>   

      <form>
        <div class="form-group">
          <label for="nic">Введите ваш ник:</label>
          <input type="text" name='nic' class="form-control">
        </div>
        
        <div class="form-group">
          <label for="summ">Сумма в Легенда: Наследие драконов (фэо-прайм) (брюли):</label>
          <input type="text" name='summ' class="form-control">
        </div>

        <div class="form-group">
          <label for="usd_summ">Сумма платежа (USD):</label>
          <input type="text" name='usd_summ' class="form-control">
        </div>

        <div class="form-group">
          <a href='/pay_complete' class='green'>Подтвердить платёж</a>
        </div>

      </form>

      <p class='gray'>
        Cтоимость 1-ой игровой единицы в проекте Легенда: Наследие драконов (фэо-прайм) составляет 20 RUR
      </p>

    </div>

    <div class='col-md-3 gray'>
      Mир Фэо. Красивый и загадочный. Мир, в котором никогда не прекращается война. Две расы ведут ожесточенную битву за право жить в этом мире. Принимайте сторону одной из них и вас ждут красивейшие бои с врагами, увлекательные походы на монстров, захватывающие квесты. В игре "Легенда: Наследие Драконов" Вы найдете все, что так любите в онлайн играх.
    </div>

  </div>
</div>  