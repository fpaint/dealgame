<div class='payment container'>  
  <? include('templates/partials/stages.php') ?>  

  <div class='orange_box'>
    <div class='orange_title'>
      <div class='left_tail'></div>
      <div class='active'>Платёж принят</div>      
      <div class='right_tail'></div>
    </div>
  </div>  

  <?= fetch_template('partials/orange_box.php', array('title'=>'Платёж принят')) ?>  

  <p style='margin-top: 30px'>
    <strong class='green'>Платёж совершён!</strong><br />
    Благодарим Вас за использование нашего сервиса!
  </p>
  
  <p style='margin: 50px 0'><a href='/' class='green'>Вернуться на главную страницу</a></p>
  </div>


</div>