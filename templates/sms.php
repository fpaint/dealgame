
<?= fetch_template('partials/orange_box.php', array('title'=>'Оплата по SMS', 'link'=>'Предыдущий шаг', 'link_href'=>'/allgames', 'class'=>'container')) ?>  

<div class='sms container'>

  <div class='alert'><strong>Внимание!</strong> Для автоматического зачисления игровой валюты через SMS, для начала выберите проект, и на следующей странице выберите
метод оплаты SMS.</div>
  <p>Выберите страну для оплаты:</p>
  <div class='row'>
    <div class='col-md-3'>
      <a href=''>Абхазия</a><br />
      <a href=''>Австралия</a><br />
      <a href=''>Австрия</a><br />
      <a href=''>Азербайджан</a><br />
      <a href=''>Албания</a><br />
      <a href=''>Аргентина</a><br />
      <a href=''>Армения</a><br />
      <a href=''>Бельгия</a><br />
      <a href=''>Болгария</a><br />
      <a href=''>Боливия</a><br />
      <a href=''>Босния и Герц-на</a><br />
      <a href=''>Бразилия</a><br />
      <a href=''>Венгрия</a><br />
      <a href=''>Венесуэла</a><br />
      <a href=''>Вьетнам</a><br />
      <a href=''>Германия</a><br />
      <a href=''>Гонконг</a><br />
      <a href=''>Греция</a><br />
      <a href=''>Грузия</a><br />
    </div>
    <div class='col-md-3'>
      <a href=''>Дания</a><br />
      <a href=''>Египет</a><br />
      <a href=''>Израиль</a><br />
      <a href=''>Индия</a><br />
      <a href=''>Индонезия</a><br />
      <a href=''>Ирландия</a><br />
      <a href=''>Испания</a><br />
      <a href=''>Италия</a><br />
      <a href=''>Казахстан</a><br />
      <a href=''>Камбоджа</a><br />
      <a href=''>Канада</a><br />
      <a href=''>Кения</a><br />
      <a href=''>Киргизия</a><br />
      <a href=''>Китай</a><br />
      <a href=''>Колумбия</a><br />
      <a href=''>Косово</a><br />
      <a href=''>Латвия</a><br />
      <a href=''>Литва</a><br />
      <a href=''>Люксембург</a><br />
    </div>
    <div class='col-md-3'>
      <a href=''>Македония</a><br />
      <a href=''>Малайзия</a><br />
      <a href=''>Марокко</a><br />
      <a href=''>Мексика</a><br />
      <a href=''>Молдавия</a><br />
      <a href=''>Нигерия</a><br />
      <a href=''>Нидерланды</a><br />
      <a href=''>Новая</a><br />
      <a href=''>Зеландия</a><br />
      <a href=''>Норвегия</a><br />
      <a href=''>Перу</a><br />
      <a href=''>Польша</a><br />
      <a href=''>Португалия</a><br />
      <a href=''>Россия</a><br />
      <a href=''>Сербия</a><br />
      <a href=''>Словакия</a><br />
      <a href=''>Словения</a><br />
      <a href=''>Таджикистан</a><br />
      <a href=''>Украина</a><br />
    </div>
    <div class='col-md-3'>
      <a href=''>Финляндия</a><br />
      <a href=''>Хорватия</a><br />
      <a href=''>Черногория</a><br />
      <a href=''>Чехия</a><br />
      <a href=''>Чили</a><br />
      <a href=''>Швейцария</a><br />
      <a href=''>Швеция</a><br />
      <a href=''>Эквадор</a><br />
      <a href=''>Эстония</a><br />
      <a href=''>ЮАР</a><br />
    </div>

  </div>

</div>